package com.ediction.app.register;

import static org.junit.jupiter.api.Assertions.assertFalse;

import com.ediction.app.exception.UserRegistrationException;
import com.ediction.app.model.UserEntity;
import com.ediction.app.model.dto.LanguageDTO;
import com.ediction.app.model.dto.RegisterDTO;
import com.ediction.app.repository.UserRepository;
import com.ediction.app.service.impl.UserServiceImpl;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.validation.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRegisterTest {

  @Autowired
  private UserServiceImpl userService;

  @Mock
  private UserRepository userRepository;

  @Before
  public void setUp() throws UserRegistrationException {
    userService.setUserRepository(userRepository);
  }

  @Test(expected = ConstraintViolationException.class)
  public void isTransactionalRegisterProcess() throws UserRegistrationException {
    Mockito.when(userRepository.save(Mockito.any(UserEntity.class))).thenThrow(new ConstraintViolationException(Collections.emptySet()));
    Mockito.spy(userService).registerUser(createTestRegistrationObject());
    Optional<UserEntity> user = userRepository
        .findByUsername(createTestRegistrationObject().getEmail());

    assertFalse(user.isPresent());

  }

  private RegisterDTO createTestRegistrationObject() {
    RegisterDTO registerDTO = new RegisterDTO();
    registerDTO.setEmail("test@testTestNewNewegister.pl");
    registerDTO.setName("Jan Kowalski");
    registerDTO.setPassword("1234");
    registerDTO.setLanguages(List.of(new LanguageDTO("0", "ENGLISH")));
    return registerDTO;
  }
}
