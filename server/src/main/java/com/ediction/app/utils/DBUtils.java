package com.ediction.app.utils;

import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

/**
 * Utils class for database configuration. It consists constants, and methods needed to correct configuration.
 */
public class DBUtils {

    // General Database constants
    private static final String SPRING_JPA_DDL_AUTO_PROP = "spring.jpa.hibernate.ddl-auto";
    private static final String SPRING_JPA_HIBERNATE_DIALECT_PROP = "spring.jpa.properties.hibernate.dialect";
    private static final String SPRING_JPA_GENERATE_DDL_PROP= "spring.jpa.generate-ddl";

    //H2 constants
    public static final String SPRING_H2_CONSOLE_ENABLE_PROP = "spring.h2.console.enabled";
    public static final String SPRING_H2_CONSOLE_PATH_PROP = "spring.h2.console.path";


    /**
     * This method returns configuration properties for database using {@param env} to getting.
     */
    public static Map<String, Object> dbProperties(final Environment env) {

        final HashMap<String, Object> properties = new HashMap<>();
        properties.put(SPRING_JPA_DDL_AUTO_PROP, env.getProperty(SPRING_JPA_DDL_AUTO_PROP));
        properties.put(SPRING_JPA_HIBERNATE_DIALECT_PROP, env.getProperty(SPRING_JPA_HIBERNATE_DIALECT_PROP));
        properties.put(SPRING_JPA_GENERATE_DDL_PROP, env.getProperty(SPRING_JPA_GENERATE_DDL_PROP));

        return properties;
    }
}
