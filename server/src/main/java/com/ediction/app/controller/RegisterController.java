package com.ediction.app.controller;

import com.ediction.app.model.dto.RegisterDTO;
import com.ediction.app.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/register-page")
public class RegisterController {

  private static final Logger LOG = LogManager.getLogger(RegisterController.class);

  private UserService userService;

  public RegisterController(final UserService userService) {
    this.userService = userService;
  }

  @PostMapping(value = "/create-account")
  public ResponseEntity createAccount(@RequestBody final RegisterDTO registerDTO) {

    try {
      userService.registerUser(registerDTO);
      LOG.info("User: " + registerDTO.getEmail() + " has been registered correctly.");
    } catch (Exception e) {
      LOG.error("Error during register User: " + registerDTO.getEmail(), e);
      return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity(HttpStatus.OK);
  }

}
