package com.ediction.app.controller;


import com.ediction.app.model.dto.LanguageDTO;
import com.ediction.app.service.LanguageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LanguageController {


  private LanguageService languageService;

  public LanguageController(LanguageService languageService) {
    this.languageService = languageService;
  }

  @GetMapping(value = "/all/languages")
  public List<LanguageDTO> getLanguages(){
    return  languageService.getAllLanguages();
  }
}
