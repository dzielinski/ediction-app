package com.ediction.app.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;


@AllArgsConstructor
@Data
public class LanguageDTO {

  @NotNull
  private String id;

  @NotNull
  private String name;
}
