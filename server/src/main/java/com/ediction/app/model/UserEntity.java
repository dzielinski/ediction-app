package com.ediction.app.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
public class UserEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NotNull
  @Column(name = "fullName")
  private String fullName;

  @NotNull
  @Email
  @Column(name = "username")
  private String username;

  @NotNull
  @Column(name = "password")
  private String password;

  @Column(name = "createdAt")
  private Date createdAt = new Date();

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<RoleEntity> roles;

  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private List<LanguageEntity> languages;

  public Long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<RoleEntity> getRoles() {
    return roles;
  }

  public void setRoles(List<RoleEntity> roles) {
    this.roles = roles;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public List<LanguageEntity> getLanguages() {
    return languages;
  }

  public void setLanguages(List<LanguageEntity> languages) {
    this.languages = languages;
  }
}
