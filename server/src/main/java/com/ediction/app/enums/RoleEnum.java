package com.ediction.app.enums;

public enum RoleEnum {

  USER("USER"), ADMIN("ADMIN");

  private final String name;

  private RoleEnum(String name) {
    this.name = name;
  }

  public String getValue() {
    return name;
  }
}
