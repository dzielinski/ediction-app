package com.ediction.app.enums;

public enum LanguageEnum {

  ENGLISH("ENGLISH"),
  GERMAN("GERMAN"),
  PORTUGUESE("PORTUGUESE");

  private final String name;

  private LanguageEnum(String name) {
    this.name = name;
  }

  public String getValue() {
    return name;
  }


}
