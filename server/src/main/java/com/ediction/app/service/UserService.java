package com.ediction.app.service;

import com.ediction.app.exception.UserRegistrationException;
import com.ediction.app.model.dto.RegisterDTO;

public interface UserService {

  /**
   * This method registers new User to system with ROLE_USER.
   */
   void  registerUser(RegisterDTO registerDTO) throws UserRegistrationException;
   
}
