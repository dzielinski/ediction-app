package com.ediction.app.service;


import com.ediction.app.model.dto.LanguageDTO;
import java.util.List;

public interface LanguageService {

 List<LanguageDTO> getAllLanguages();
}
