package com.ediction.app.service.impl;

import com.ediction.app.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    return userRepository
        .findByUsername(username)
        .map(u -> new org.springframework.security.core.userdetails.User(
            u.getUsername(),
            u.getPassword(),
            AuthorityUtils.createAuthorityList(
                u.getRoles()
                    .stream()
                    .map(r -> r.getName().toUpperCase())
                    .collect(Collectors.toList())
                    .toArray(new String[]{}))))
        .orElseThrow(() -> new UsernameNotFoundException("No user with "
            + "the name " + username + "was found in the database"));
  }

  ;
}
