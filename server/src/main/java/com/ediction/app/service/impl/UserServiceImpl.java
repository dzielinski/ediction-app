package com.ediction.app.service.impl;

import com.ediction.app.model.LanguageEntity;
import com.ediction.app.model.dto.LanguageDTO;
import com.ediction.app.repository.LanguageRepository;
import com.ediction.app.repository.RoleRepository;
import com.ediction.app.repository.UserRepository;
import com.ediction.app.enums.RoleEnum;
import com.ediction.app.exception.UserRegistrationException;
import com.ediction.app.model.RoleEntity;
import com.ediction.app.model.UserEntity;
import com.ediction.app.model.dto.RegisterDTO;
import com.ediction.app.service.UserService;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

  private static final Logger LOG = LogManager.getLogger(UserServiceImpl.class);

  private UserRepository userRepository;
  private RoleRepository roleRepository;
  private LanguageRepository languageRepository;
  private BCryptPasswordEncoder passwordEncoder;

  @Override
  @Transactional(propagation= Propagation.REQUIRED)
  public void registerUser(final RegisterDTO registerDTO)
      throws UserRegistrationException {
    final Optional<UserEntity> user = userRepository.findByUsername(registerDTO.getEmail());
    if (user.isPresent()) {
      throw new UserRegistrationException("User with email " + registerDTO.getEmail() + " exists");
    }

    final UserEntity userEntity = transferDTOToUser(registerDTO);
    userRepository.save(userEntity);
    LOG.info("Saved User with email: " + registerDTO.getEmail());
  }

  private UserEntity transferDTOToUser(final RegisterDTO registerDTO)
      throws UserRegistrationException {
    final UserEntity user = new UserEntity();
    user.setFullName(registerDTO.getName());
    user.setUsername(registerDTO.getEmail());
    user.setRoles(addUserRole());
    user.setPassword(encodePassword(registerDTO.getPassword()));
    user.setLanguages(setLanguagesForNewUser(registerDTO.getLanguages()));
    return user;
  }

  private List<LanguageEntity> setLanguagesForNewUser(final List<LanguageDTO> languages) {
    final Iterator<LanguageDTO> iterator = languages.iterator();
    final Set<Long> idsLanguage = new HashSet<>();
    while(iterator.hasNext()){
      idsLanguage.add(Long.valueOf(iterator.next().getId()));
    }

    return languageRepository.findAllById(idsLanguage);
  }

  private String encodePassword(final String password) {
    return passwordEncoder.encode(password);
  }

  private List<RoleEntity> addUserRole() throws UserRegistrationException {
    final Optional<RoleEntity> userRole = Optional.ofNullable(roleRepository.getByName(RoleEnum.USER.name()));
     return List.of(userRole.orElseThrow(() -> new UserRegistrationException(
          "Cannot assign " + RoleEnum.USER + " Role to user, because this role doesn't exist.")));
  }

  @Autowired
  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Autowired
  public void setRoleRepository(RoleRepository roleRepository) {
    this.roleRepository = roleRepository;
  }

  @Autowired
  public void setLanguageRepository(LanguageRepository languageRepository) {
    this.languageRepository = languageRepository;
  }

  @Autowired
  public void setPasswordEncoder(
      BCryptPasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }
}
