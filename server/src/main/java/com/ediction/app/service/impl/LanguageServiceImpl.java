package com.ediction.app.service.impl;

import com.ediction.app.model.dto.LanguageDTO;
import com.ediction.app.repository.LanguageRepository;
import com.ediction.app.service.LanguageService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements LanguageService {

  @Autowired
  private LanguageRepository languageRepository;

  @Override
  public List<LanguageDTO> getAllLanguages() {
    return languageRepository.findAll().stream()
        .map(language -> new LanguageDTO(language.getId().toString(), language.getName())).collect(
            Collectors.toList());
  }
}
