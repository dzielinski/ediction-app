package com.ediction.app.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationConfig extends AuthorizationServerConfigurerAdapter {


  @Autowired
  @Qualifier("authenticationManagerBean")
  private AuthenticationManager authenticationManager;

  @Autowired
  @Qualifier("mySQLDataSource")
  DataSource dataSource;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public void configure(
      AuthorizationServerSecurityConfigurer oauthServer)
      throws Exception {
    oauthServer
        .tokenKeyAccess("permitAll()")
        .checkTokenAccess("isAuthenticated()")
        .passwordEncoder(passwordEncoder);
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients)
      throws Exception {

    clients.inMemory()
        .withClient("client")
        .secret("secret")
        .authorizedGrantTypes("password")
        .scopes("read", "write", "trust")
        .accessTokenValiditySeconds(60 * 60 * 8)
        .resourceIds("oauth2-resource");
  }

  @Override
  public void configure(
      AuthorizationServerEndpointsConfigurer endpoints)
      throws Exception {

    endpoints
        .tokenStore(tokenStoreOAuth())
        .authenticationManager(authenticationManager);
  }

  @Bean
  public TokenStore tokenStoreOAuth() {
    return new JdbcTokenStore(dataSource);
  }


}
