package com.ediction.app.configuration;

import com.ediction.app.utils.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;

@Profile("h2")
@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application-h2.properties")
@EnableJpaRepositories(basePackages = {"com.ediction.app"})
public class H2DbConfig {

  @Autowired
  private Environment env;

  @Profile("h2")
  @Bean(name = "h2DataSource")
  public DataSource dataSource() {

    DriverManagerDataSource h2DataSource = new DriverManagerDataSource();
    h2DataSource.setDriverClassName(env.getProperty("h2.datasource.driver-class-name"));
    h2DataSource.setUrl(env.getProperty("h2.datasource.url"));
    h2DataSource.setUsername(env.getProperty("h2.datasource.username"));
    h2DataSource.setPassword(env.getProperty("h2.datasource.password"));

    return h2DataSource;
  }

  @Profile("h2")
  @Bean(name = "entityManagerFactory")
  public LocalContainerEntityManagerFactoryBean
  h2EntityManagerFactory(
      EntityManagerFactoryBuilder builder, @Qualifier("h2DataSource") DataSource dataSource) {
    return builder.dataSource(dataSource)
        .packages("com.ediction.app")
        .properties(getH2Properties())
        .persistenceUnit("h2")
        .build();
  }

  private Map<String, ?> getH2Properties() {
    Map<String, Object> properties = DBUtils.dbProperties(env);
    properties.put(DBUtils.SPRING_H2_CONSOLE_ENABLE_PROP,
        env.getProperty(DBUtils.SPRING_H2_CONSOLE_ENABLE_PROP));
    properties.put(DBUtils.SPRING_H2_CONSOLE_PATH_PROP,
        env.getProperty(DBUtils.SPRING_H2_CONSOLE_PATH_PROP));

    return properties;
  }

  @Profile("h2")
  @Bean(name = "transactionManager")
  public PlatformTransactionManager h2TransactionManager(
      @Qualifier("entityManagerFactory") EntityManagerFactory h2EntityManagerFactory) {
    return new JpaTransactionManager(h2EntityManagerFactory);
  }
}
