package com.ediction.app.configuration;

import com.ediction.app.utils.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;

@Profile("mysql")
@PropertySource("classpath:application-mysql.properties")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.ediction.app"})
public class MySQLConfig {

  @Autowired
  private Environment env;

  @Value("classpath:schema.sql")
  private Resource schemaScript;

  @Profile("mysql")
  @Bean(name = "mySQLDataSource")
  public DataSource dataSource() {

    DriverManagerDataSource mysqlDataSource = new DriverManagerDataSource();
    mysqlDataSource.setDriverClassName(env.getProperty("mysql.datasource.driver-class-name"));
    mysqlDataSource.setUrl(env.getProperty("mysql.datasource.url"));
    mysqlDataSource.setUsername(env.getProperty("mysql.datasource.username"));
    mysqlDataSource.setPassword(env.getProperty("mysql.datasource.password"));

    return mysqlDataSource;
  }

  @Profile("mysql")
  @Bean(name = "entityManagerFactory")
  public LocalContainerEntityManagerFactoryBean mySQLEntityManagerFactory(
      EntityManagerFactoryBuilder builder,
      @Qualifier("mySQLDataSource") DataSource dataSource
  ) {

    return builder.dataSource(dataSource)
        .packages("com.ediction.app")
        .persistenceUnit("mySQL")
        .properties(getMySQLProperties())
        .build();
  }

  private Map<String, ?> getMySQLProperties() {
    return DBUtils.dbProperties(env);
  }

  @Profile("mysql")
  @Bean(name = "transactionManager")
  public PlatformTransactionManager mySQLTransactionManager(
      @Qualifier("entityManagerFactory") EntityManagerFactory
          mySQLEntityManagerFactory
  ) {
    return new JpaTransactionManager(mySQLEntityManagerFactory);
  }

  @Bean
  public TokenStore tokenStore() {
    return new JdbcTokenStore(dataSource());
  }
}
