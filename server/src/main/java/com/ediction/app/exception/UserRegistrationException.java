package com.ediction.app.exception;

public class UserRegistrationException extends Exception {

  public UserRegistrationException() {
  }

  public UserRegistrationException(String message, Throwable cause) {
    super(message, cause);
  }

  public UserRegistrationException(String message) {
    super(message);
  }
}
