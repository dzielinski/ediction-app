package com.ediction.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableResourceServer
@EnableTransactionManagement
public class EdictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdictionApplication.class, args);
	}
}
