package com.ediction.app.repository;

import com.ediction.app.model.LanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public interface LanguageRepository extends JpaRepository<LanguageEntity, Long> {

}
