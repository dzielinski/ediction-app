import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(
      private _router: Router, private _http: HttpClient, private cookieService: CookieService) {

  }

  obtainAccessToken(loginData) {
    const params = new URLSearchParams();
    params.append('username', loginData.username);
    params.append('password', loginData.password);
    params.append('grant_type', 'password');
    params.append('client_id', 'client');

    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic Y2xpZW50OnNlY3JldA=='
    });

    const options = {headers: headers};
    this._http.post(environment.ENV + '/oauth/token', params.toString(), options)
    .subscribe(
        data => this.saveToken(data),
        err => alert('Invalid Username or Password!'));
  }

  saveToken(token) {
    const expireDate = new Date().getTime() + (1000 * token.expires_in);
    this.cookieService.set('access_token', token.access_token, expireDate);
    this._router.navigate(['/home']);
  }

  getResource(resourceUrl): any {
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + this.cookieService.get('access_token')
    });
    const options = {headers: headers};
    return this._http.get(resourceUrl, options).subscribe(number => number);
  }

  checkCredentials() {
    if (!this.cookieService.check('access_token')) {
      this._router.navigate(['/register']);
    }
  }

  logout() {
    this.cookieService.delete('access_token');
    this._router.navigate(['/register']);
  }
}
