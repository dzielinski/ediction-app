import {Component, OnInit} from '@angular/core';
import {AppService} from '../app.service';
import {LoginForm} from '../../shared/login-page/LoginForm';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {


  loginForm = new LoginForm();

  constructor(private appService: AppService) {
  }

  login() {
    this.appService.obtainAccessToken(this.loginForm);
  }

  ngOnInit(): void {
  }

}
