import {Component, OnInit} from '@angular/core';
import {RegistrationPageService} from './registration-page.service';
import {RegisterForm} from '../../shared/register-page/RegisterForm';
import {Language} from '../../shared/Language';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController} from '@ionic/angular';


@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {

  constructor(private registrationService: RegistrationPageService,
              private alertController: AlertController) {
  }

  registerForm = new RegisterForm();

  languages: Language[] = [];

  form = new FormGroup({
    languages: new FormControl('', [ Validators.required ]),
    name: new FormControl(this.registerForm.name, Validators.required),
    password: new FormControl(this.registerForm.password, Validators.required),
    confirmPassword: new FormControl(this.registerForm.confirmPassword, Validators.required),
    email: new FormControl(this.registerForm.email, [Validators.required, Validators.email])
  });

  getAllLanguages() {
    this.registrationService.getLanguages()
    .subscribe((data: Language[]) => this.languages = data);
  }

  ngOnInit() {
    this.registerForm.languages = null;
    this.getAllLanguages();
  }

  checkPasswords() {
    const pass = this.registerForm.password;
    const confirmPass = this.registerForm.confirmPassword;

    if (pass !== confirmPass) {
      this.confirmationPasswordAlert();
      return false;
    }

    return true;
  }

  createAccount() {
    if (this.isValidForm()) {
      this.registrationService.createAccount(this.registerForm);
    }
  }

  isValidForm() {
    return this.checkPasswords();
  }

  async confirmationPasswordAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Confirmation Password',
      message: 'Confirmed password is not correct.',
      buttons: ['OK']
    });

    await alert.present();
  }
}


