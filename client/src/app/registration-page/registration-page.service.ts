import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {Language} from '../../shared/Language';


@Injectable({
  providedIn: 'root'
})
export class RegistrationPageService {

  constructor(private _http: HttpClient, private router: Router, private alertController: AlertController) {
  }


  createAccount(registerDTO) {
    this._http.post(environment.ENV_LOCALHOST + '/register-page/create-account', registerDTO)
    .subscribe(() => this.redirectToLoginPageIfOK(), () => {
      this.registerErrorMessage();
    });
  }


  redirectToLoginPageIfOK() {
    this.registerSuccessfulMessage();
    this.router.navigateByUrl('/login');
  }

  getLanguages() {
    return this._http
    .get<Language[]>(environment.ENV_LOCALHOST + '/all/languages');
  }


  async registerErrorMessage() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Register Error',
      message: 'Something went wrong, we could not register this account, please contact with administrator or try again later.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async registerSuccessfulMessage() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Register Successful!',
      message: 'Register successful! We sent you activation link to your email address, please activate account via this link.',
      buttons: ['OK']
    });

    await alert.present();
  }


}
