import {Language} from '../Language';

export class RegisterForm {

  public name: string;
  public email: string;
  public password: string;
  public confirmPassword: string;
  public languages = new Language();
}
